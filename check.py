#!/usr/bin/env python

import sys

#Example line from the file
#malloc	804b170	ffffffffffffffff	../test.c	main	8

class Allocation():
	def __init__(self, action, newptr, oldptr, fileName, function, lineno):
		self.action = action
		self.newptr = newptr
		self.oldptr = oldptr
		self.fileName = fileName
		self.function = function
		self.lineno = lineno

	def toString(self):
		return self. action+" "+self.newptr+" "+self.oldptr+" "+self.fileName+" "+self.function+" "+self.lineno

class AllocationTable():
	def __init__(self):
		self.table = dict()

	def free(self, alloc):

		ptr = alloc.newptr
		if alloc.action == "realloc":
			ptr = alloc.oldptr

		if ptr in self.table:
			del(self.table[ptr])
		else:
			if alloc.action == "realloc" and ptr != "(nil)":
				print("Invalid Free: " + alloc.toString())
		
	def alloc(self, alloc):
		if alloc.newptr not in self.table:
			self.table[alloc.newptr] = alloc
		else:
			print("Heap Corruption: " + alloc.toString() + " already allocated: " + self.table[alloc.newptr].toString())

allocationTable = AllocationTable()
inputFile = sys.argv[1]
inFile = open(inputFile)

for line in inFile:
	chunks = line.split()
	alloc = Allocation(chunks[0], chunks[1], chunks[2], chunks[3], chunks[4], chunks[5])

	if alloc.action == "free":
		allocationTable.free(alloc)		
		
	elif alloc.action == "realloc":
		allocationTable.free(alloc)
		allocationTable.alloc(alloc)

	else:
		allocationTable.alloc(alloc)

for key in allocationTable.table:
	a = allocationTable.table[key]
	print("Leak: " + a.toString())

inFile.close()
