#include <stdio.h>

#define CHECKMEM
#include "memcheck.h"

int main(int argc, char *argv[]) {

	void *p0 = malloc(1024);
	void *leak0 = malloc(1024);

	void *p1 = calloc(12, 1024);
	void *leak1 = calloc(12, 1024);

	void *p2 = realloc(NULL, 1024);
	void *leak2 = realloc(NULL, 1024);

	void *rlck0 = realloc(malloc(1024), 2048);
	void *rlck_leak0 = realloc(malloc(1024), 2048);

	free(p0);
	free(p1);
	free(p2);
	free(rlck0);
}
