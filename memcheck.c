#include <stdio.h>
#include <string.h>
#include <pthread.h>

/* Must include last since this redefines stuff and turn off the redefinitions*/
#ifdef CHECKMEM
#undef CHECKMEM
#endif

#include "memcheck.h"

#define MEMCHECK_LOGFILE ".memlog"

typedef enum action action;

enum action {
	action_free = 0,
	action_calloc,
	action_malloc,
	action_realloc,
	action_strdup
};

static pthread_mutex_t _lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_once_t _oc = PTHREAD_ONCE_INIT;

/* 
 * TODO: Is this really necessary?
 * Maybe timestamping the logs is better than using locks
 * to TRY and gaurentee the order the logs. Is thier a portable
 * high precision timestamp?
 */
#define lock()    pthread_mutex_lock(&_lock);
#define unlock()  pthread_mutex_unlock(&_lock);
#define init()    pthread_once(&_oc, memcheck_init);

static FILE *fp = NULL;
static char *action_map[] = {"free", "calloc", "malloc", "realloc", "strdup"};

static void writelog(FILE *f, const char *file, const char *func, int lineno, action act, void *ptr, void *oldptr) {
	/* stdio should be thread safe*/
	fprintf(f, "%s\t%p\t%p\t%s\t%s\t%d\n", action_map[act], ptr, oldptr, file, func, lineno);
	/* New line at the end should cause a flush, being paranoid here */
	fflush(f);
}

static void memcheck_init() {
	fp = fopen(MEMCHECK_LOGFILE, "w+");
}

void *_memcheck_malloc(const char *filename, const char *func, int lineno, size_t size) {
	lock();
	init();
	void *p = malloc(size);
	writelog(fp, filename, func, lineno, action_malloc, p, NULL);
	unlock();
	return p;
}

void *_memcheck_realloc(const char *filename, const char *func, int lineno, void *ptr, size_t size) {
	lock();
	init();
	void *p = realloc(ptr, size);
	writelog(fp, filename, func, lineno, action_realloc, p, ptr);
	unlock();
	return p;
}

void *_memcheck_calloc(const char *filename, const char *func, int lineno, size_t nmemb, size_t size) {
	lock();
	init();
	void *p = calloc(nmemb, size);
	writelog(fp, filename, func, lineno, action_calloc, p, NULL);
	unlock();
	return p;
}

void _memcheck_free(const char *filename, const char *func, int lineno, void *ptr) {
	lock();
	init();
	free(ptr);
	writelog(fp, filename, func, lineno, action_free, ptr, NULL);
	unlock();
	return;
}

char *_memcheck_strdup(const char *filename, const char *func, int lineno, const char *str) {
	lock();
	init();
	void *p = strdup(str);
	writelog(fp, filename, func, lineno, action_strdup, p, NULL);
	unlock();
	return p;
}


char *_memcheck_strndup(const char *filename, const char *func, int lineno, const char *str, size_t size) {
	lock();
	init();
	void *p = strndup(str, size);
	writelog(fp, filename, func, lineno, action_strdup, p, NULL);
	unlock();
	return p;
}
