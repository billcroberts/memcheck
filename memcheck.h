#ifndef _MEMCHECK_H_
#define _MEMCHECK_H_

#include <stdlib.h>

#ifdef _cplusplus
 extern "C" {
#endif

#ifdef CHECKMEM
  #define malloc(s)      _memcheck_malloc(__FILE__, __func__, __LINE__, s)
  #define realloc(o, s)  _memcheck_realloc(__FILE__, __func__, __LINE__, o, s)
  #define calloc(n, s)   _memcheck_calloc(__FILE__, __func__, __LINE__, n, s)
  #define free(p)        _memcheck_free(__FILE__, __func__, __LINE__, p)

  #define strdup(str)    _memcheck_strdup(__FILE__, __func__, __LINE__, str)

  extern void *_memcheck_malloc(const char *filename, const char *func, int lineno, size_t size);
  extern void *_memcheck_realloc(const char *filename, const char *func, int lineno, void *ptr, size_t size);
  extern void *_memcheck_calloc(const char *filename, const char *func, int lineno, size_t nmemb, size_t size);
  extern void _memcheck_free(const char *filename, const char *func, int lineno, void *ptr);

  extern char *_memcheck_strdup(const char *filename, const char *func, int lineno, char *str);
  #endif

#ifdef _cplusplus
}
#endif
#endif
